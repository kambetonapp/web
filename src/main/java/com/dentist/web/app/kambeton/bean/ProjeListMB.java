package com.dentist.web.app.kambeton.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.repo.proje.FirmaRepository;
import com.vassaf.core.repo.proje.LokasyonRepository;
import com.vassaf.core.service.FirmaService;
import com.vassaf.core.service.ProjeService;

/**
 * @author
 *
 */
@Named       // Named olarak anote ettiğimiz sınıfı .xhtml sayfalarında kullanabiliriz.
@ViewScoped  // Sınıfın ram'de kalma ömrü, ekran kapandığında biter. 
public class ProjeListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	ProjeService service;
	
	@Autowired
	FirmaRepository firmaRepository;
	
	@Autowired
	LokasyonRepository lokasyonRepository;

	LazyDataModel<Proje> projeler;

	Proje secilenProje;

	
	@PostConstruct
	public void initDataModel() {
		
		
		projeler = new LazyDataModel<Proje>() {
			
			private static final long serialVersionUID = -6109349694404018774L;

			@Override
			public List<Proje> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Proje> page;

				page = service.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Proje> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Proje getRowData(String key) {
				return service.findById(new Long(key)).get();
			}
		};
	}


	public void delete() throws IOException {
		service.delete(secilenProje);
		addDetailMessage(secilenProje.getIsim() + " isimli proje başarı ile silindi");
		secilenProje = new Proje();
		Faces.redirect("/musteri-kaydi/proje-list.jsf");
	}



	public void onRowSelect(SelectEvent event) {
		secilenProje = (Proje) event.getObject();
	}


	public void showNewView() {
		this.secilenProje = null;
		this.secilenProje = new Proje();
	}


	public ProjeService getService() {
		return service;
	}


	public void setService(ProjeService service) {
		this.service = service;
	}


	public LazyDataModel<Proje> getProjeler() {
		return projeler;
	}


	public void setProjeler(LazyDataModel<Proje> projeler) {
		this.projeler = projeler;
	}


	public Proje getSecilenProje() {
		return secilenProje;
	}


	public void setSecilenProje(Proje secilenProje) {
		this.secilenProje = secilenProje;
	}
	
	
}
