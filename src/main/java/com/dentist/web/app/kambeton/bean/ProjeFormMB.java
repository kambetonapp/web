package com.dentist.web.app.kambeton.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;

import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Doctor;
import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.service.FirmaService;
import com.vassaf.core.service.LokasyonService;
import com.vassaf.core.service.ProjeService;

/**
 * Created by @berken on 15.01.2020. proje-form.jsf sayfasının bean'idir.
 */
@Named
@ViewScoped
public class ProjeFormMB implements Serializable {

	private static final long serialVersionUID = 2057222688048224512L;
	
	@Autowired
	ProjeService projeService;

	@Autowired
	LokasyonService lokasyonService;
	
	@Autowired
	FirmaService firmaService;
	
	Long secilenFirmaId;
	
	Long secilenLokasyonId;
	
	Long secilenProjeId;
	
	Firma secilenFirma;
	
	Proje secilenProje;
	
	Lokasyon secilenLokasyon;
	
	boolean saved;

	@PostConstruct
	public void init() {
		
		// if ajax request : return
		if (has(secilenFirmaId) && secilenFirmaId != 0) {
			secilenFirma = firmaService.findById(secilenFirmaId).get();
		} else {
			secilenFirma = new Firma();
		}
		
		// if ajax request : return
		if (has(secilenLokasyonId) && secilenLokasyonId != 0) {
			secilenLokasyon = lokasyonService.findById(secilenLokasyonId).get();
		} else {
			secilenLokasyon = new Lokasyon();
		}
		
		// if ajax request : return
		if (has(secilenProjeId) && secilenProjeId != 0) {
			secilenProje = projeService.findById(secilenProjeId).get();
		} else {
			secilenProje = new Proje();
		}

	}




	public void save() throws IOException {
		
		String msg;

		if (this.secilenProje != null) {
			System.err.println("Seçilen Lokasyon : " +secilenLokasyon);
			
			projeService.save(this.secilenProje);
			
			msg = this.secilenProje.getIsim() + " Adlı Proje " + secilenFirma.getIsim() + " Adlı Firmanın "
					+ secilenLokasyon.getIsim() + " Adlı Lokasyonuna Kaydedildi." ;
		} else {
			projeService.save(this.secilenProje);
			
			msg = this.secilenProje.getIsim() + " Adlı Proje " + secilenFirma.getIsim() + " Adlı Firmanın "
					+ secilenLokasyon.getIsim() + " Adlı Lokasyonuna Başarıyla Güncellendi.";
			
		}
		addDetailMessage(msg);
		Faces.redirect("/musteri-kaydi/proje-list.jsf");
		
	}
	
	public List<Lokasyon> getLokasyonlar() {
		if (secilenFirmaId == null) {
		return null;
		}
		else {
		System.out.println("secilen f:"+ secilenFirma);
		return lokasyonService.getLokasyonlar(secilenFirma);
		}
	}
	
	
	public List<Firma> getFirmalar() {
		return firmaService.findAll();
	}
	
	
	public void clear() {
		secilenProje = new Proje();
		setSecilenFirmaId(null);
		setSecilenLokasyonId(null);
	}



	public Proje getSecilenProje() {
		return secilenProje;
	}



	public void setSecilenProje(Proje secilenProje) {
		this.secilenProje = secilenProje;
	}



	public Lokasyon getSecilenLokasyon() {
		return secilenLokasyon;
	}



	public void setSecilenLokasyon(Lokasyon secilenLokasyon) {
		this.secilenLokasyon = secilenLokasyon;
	}



	public ProjeService getProjeService() {
		return projeService;
	}



	public void setProjeService(ProjeService projeService) {
		this.projeService = projeService;
	}



	public LokasyonService getLokasyonService() {
		return lokasyonService;
	}



	public void setLokasyonService(LokasyonService lokasyonService) {
		this.lokasyonService = lokasyonService;
	}



	public boolean isSaved() {
		return saved;
	}



	public void setSaved(boolean saved) {
		this.saved = saved;
	}
	
	public FirmaService getFirmaService() {
		return firmaService;
	}



	public void setFirmaService(FirmaService firmaService) {
		this.firmaService = firmaService;
	}



	public Long getSecilenFirmaId() {
		return secilenFirmaId;
	}


	// Seçim yaptığımzda jsf bu metodu çağırıyor
	// Biz de gelen id ile veritabanından sorgu yapıp secilenFirma ya atıyoruz.
	// buradaki get fonksiyonu obje olarak mı çekiyor?
	// Optional diye bir sınıf var . Bu sınıf bize eğer veriyi bulamazsak exception fırlatmamızı sağlıyor. Onunla uğraşmadan direk get() deyip değeri alıyoruz.
	public void setSecilenFirmaId(Long secilenFirmaId) {
		if (secilenFirmaId == null) {
			
		}
		else {
		System.err.println("Firma secildi " + secilenFirmaId);
		this.secilenFirma = firmaService.findById(Long.valueOf(secilenFirmaId)).get();
		this.secilenFirmaId = secilenFirmaId;
		}
	}




	public Long getSecilenLokasyonId() {
		return secilenLokasyonId;
	}




	public void setSecilenLokasyonId(Long secilenLokasyonId) {
		if (secilenLokasyonId == null) {
			
		}
		else {
		System.err.println("Lokasyon secildi " + secilenLokasyonId);
		this.secilenLokasyon = lokasyonService.findById(Long.valueOf(secilenLokasyonId)).get();
		this.secilenLokasyonId = secilenLokasyonId;
		secilenProje.setLokasyon(secilenLokasyon);
		}
	}




	public Firma getSecilenFirma() {
		return secilenFirma;
	}




	public void setSecilenFirma(Firma secilenFirma) {
		this.secilenFirma = secilenFirma;
	}
	
	

	public Long getSecilenProjeId() {
		return secilenProjeId;
	}




	public void setSecilenProjeId(Long secilenProjeId) {
		this.secilenProjeId = secilenProjeId;
	}


	

}
