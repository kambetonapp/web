package com.dentist.web.app.kambeton.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;

import java.util.Map;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.model.agirlik.listesi.urun.Urun;

import com.vassaf.core.service.ProjeService;
import com.vassaf.core.service.UrunService;


/**
 * @author
 *
 */
@Named       // Named olarak anote ettiğimiz sınıfı .xhtml sayfalarında kullanabiliriz.
@ViewScoped  // Sınıfın ram'de kalma ömrü, ekran kapandığında biter. 
public class UrunListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	
	@Autowired
	ProjeService projeService;
	
	@Autowired
	UrunService urunService;

	LazyDataModel<Urun> urunler;
	
	List<Proje> projeler;
	
	Urun secilenUrun;

	Proje secilenProje;
	
	Long secilenProjeId;

	
	@PostConstruct
	public void initDataModel() {
		
		
		urunler = new LazyDataModel<Urun>() {
			
			private static final long serialVersionUID = -6109349694404018774L;

			@Override
			public List<Urun> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Urun> page;

				page = urunService.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Urun> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Urun getRowData(String key) {
				return urunService.findById(new Long(key)).get();
			}
		};
	}


	public void delete() throws IOException {
		urunService.delete(secilenUrun);
		addDetailMessage(secilenUrun.getIsim() + " isimli urun başarı ile silindi");
		secilenUrun = new Urun();
		Faces.redirect("/musteri-kaydi/urun-list.jsf");
	}



	public void onRowSelect(SelectEvent event) {
		secilenUrun = (Urun) event.getObject();
	}


	public void showNewView() {
		this.secilenUrun = null;
		this.secilenUrun = new Urun();
	}
	
	
	
	
	public List<Proje> getProjeler() {
		
		System.err.println("projeler veritabanından çekiliyor....");
		
		System.err.println("çekilen projeler  " + projeService.findAll());
		return projeService.findAll();
		
	}


	public UrunService getUrunService() {
		return urunService;
	}


	public void setUrunService(UrunService urunService) {
		this.urunService = urunService;
	}


	public LazyDataModel<Urun> getUrunler() {
		return urunler;
	}


	public void setUrunler(LazyDataModel<Urun> urunler) {
		this.urunler = urunler;
	}


	public Urun getSecilenUrun() {
		return secilenUrun;
	}


	public void setSecilenUrun(Urun secilenUrun) {
		this.secilenUrun = secilenUrun;
	}


	public Proje getSecilenProje() {
		return secilenProje;
	}


	public void setSecilenProje(Proje secilenProje) {
		this.secilenProje = secilenProje;
	}


	public ProjeService getProjeService() {
		return projeService;
	}


	public void setProjeService(ProjeService projeService) {
		this.projeService = projeService;
	}


	public Long getSecilenProjeId() {
		return secilenProjeId;
	}


	public void setSecilenProjeId(Long secilenProjeId) {
		this.secilenProjeId = secilenProjeId;
	}


	public void setProjeler(List<Proje> projeler) {
		this.projeler = projeler;
	}


	
	
}
