package com.dentist.web.app.kambeton.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.service.FirmaService;
import com.vassaf.core.service.LokasyonService;

/**
 * @author @berken
 *
 */
@Named       // Named olarak anote ettiğimiz sınıfı .xhtml sayfalarında kullanabiliriz.
@ViewScoped  // Sınıfın ram'de kalma ömrü, ekran kapandığında biter. 
public class LokasyonMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	FirmaService firmaService;
	
	@Autowired
	LokasyonService lokasyonService;

	LazyDataModel<Firma> firmalar;
	
	List<Lokasyon> lokasyonlar;
	
	Firma secilenFirma;
	
	List<Firma> filteredValue;
	

	Lokasyon secilenLokasyon;

	
	
	
	@PostConstruct
	public void initDataModel() {
		firmalar = new LazyDataModel<Firma>() {
			
			private static final long serialVersionUID = -6109349694404018774L;

			@Override
			public List<Firma> load(int first, 
									int pageSize, 
									String sortField, 
									SortOrder sortOrder,
									Map<String, Object> filters) 
			{
				
				Page<Firma> page;
				
				Sort sort = Sort.by(Direction.DESC, "id");
				
				if(sortField != null) {
					sort = sortOrder == SortOrder.ASCENDING ? Sort.by(Direction.ASC, sortField) : Sort.by(Direction.DESC, sortField);
				}
				
				
				if (null != filters && filters.get("isim") != null) {
					System.err.println("Gelen Filter " + filters.get("isim"));
					
					page = firmaService.findByIsim(String.valueOf(filters.get("isim")), PageRequest.of(first / pageSize, pageSize, sort));
					
					List<Firma> list = page.getContent();

					setRowCount((int) page.getTotalElements());

					return list;
				}
				
				

				page = firmaService.findAll(PageRequest.of(first / pageSize, pageSize, sort));

				List<Firma> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Firma getRowData(String key) {
				return firmaService.findById(new Long(key)).get();
			}
		};
	}


	public void firmaSil() throws IOException {
		firmaService.delete(secilenFirma);
		
		addDetailMessage(secilenFirma.getIsim() + " isimli firma başarı ile silindi");
		secilenFirma = new Firma();
		Faces.redirect("/musteri-kaydi/firma-list.jsf");
	}



	public void onRowSelect(SelectEvent event) {
		secilenFirma = (Firma) event.getObject();
		
		lokasyonlar = lokasyonService.getLokasyonlar(secilenFirma);
		
		
//		this.lokasyonlar = new ArrayList<>();
//		
//		Lokasyon l = new Lokasyon();
//		
//		l.setFirma(secilenFirma);
//		l.setIsim(l.getIsim());
//		
//		System.err.println("Lokasyonlar : "+ this.lokasyonlar);
		
	}
	
	public void lokasyonSil(Lokasyon l) throws IOException {
		
		lokasyonService.delete(l);
		addDetailMessage(secilenFirma.getIsim() + " firmasına ait " + l.getIsim() + " lokasyonu başarı ile silindi.");
		
		Faces.redirect("/musteri-kaydi/lokasyon-list.jsf");
	}

	public void save() throws IOException {
		if (this.secilenLokasyon != null) {
			
			secilenLokasyon.setFirma(secilenFirma);
			lokasyonService.save(this.secilenLokasyon);
			addDetailMessage(this.secilenLokasyon.getIsim() + " Adlı Lokasyon " + secilenFirma.getIsim() + " Firmasına Kaydedildi.");
			Faces.redirect("/musteri-kaydi/lokasyon-list.jsf");
		}
	}

	public void showNewView() {
		this.secilenLokasyon = null;
		this.secilenLokasyon = new Lokasyon();
	}
	
	
	
	public void showUpdateLokasyonDialog(Lokasyon l) {
		
		System.err.println("secilen lok"+l);
		this.secilenLokasyon =l;
	}

	
	public FirmaService getService() {
		return firmaService;
	}


	public void setService(FirmaService service) {
		this.firmaService = service;
	}


	public LazyDataModel<Firma> getFirmalar() {
		return firmalar;
	}


	public void setFirmalar(LazyDataModel<Firma> firmalar) {
		this.firmalar = firmalar;
	}


	public Firma getSecilenFirma() {
		return secilenFirma;
	}
	
	


	public List<Firma> getFilteredValue() {
		return filteredValue;
	}


	public void setFilteredValue(List<Firma> filteredValue) {
		this.filteredValue = filteredValue;
	}


	public void setSecilenFirma(Firma secilenFirma) {
		this.secilenFirma = secilenFirma;
	}


	public List<Lokasyon> getLokasyonlar() {
		return lokasyonlar;
	}


	public void setLokasyonlar(List<Lokasyon> lokasyonlar) {
		this.lokasyonlar = lokasyonlar;
	}


	public FirmaService getFirmaService() {
		return firmaService;
	}


	public void setFirmaService(FirmaService firmaService) {
		this.firmaService = firmaService;
	}


	public LokasyonService getLokasyonService() {
		return lokasyonService;
	}


	public void setLokasyonService(LokasyonService lokasyonService) {
		this.lokasyonService = lokasyonService;
	}


	public Lokasyon getSecilenLokasyon() {
		return secilenLokasyon;
	}


	public void setSecilenLokasyon(Lokasyon secilenLokasyon) {
		this.secilenLokasyon = secilenLokasyon;
	}
	
	
	
	
}
