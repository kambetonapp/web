package com.dentist.web.app.kambeton.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.service.FirmaService;

/**
 * @author @berken
 *
 */
@Named       // Named olarak anote ettiğimiz sınıfı .xhtml sayfalarında kullanabiliriz.
@ViewScoped  // Sınıfın ram'de kalma ömrü, ekran kapandığında biter. 
public class FirmaMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	FirmaService service;

	LazyDataModel<Firma> firmalar;

	
	Firma secilenFirma;

	
	@PostConstruct
	public void initDataModel() {
		firmalar = new LazyDataModel<Firma>() {
			
			private static final long serialVersionUID = -6109349694404018774L;

			@Override
			public List<Firma> load(int first, 
									int pageSize, 
									String sortField, 
									SortOrder sortOrder,
									Map<String, Object> filters) 
			{
				Page<Firma> page;

				page = service.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Firma> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Firma getRowData(String key) {
				return service.findById(new Long(key)).get();
			}
		};
	}


	public void delete() throws IOException {
		service.delete(secilenFirma);
		addDetailMessage(secilenFirma.getIsim() + " isimli firma başarı ile silindi");
		secilenFirma = new Firma();
		Faces.redirect("/musteri-kaydi/firma-list.jsf");
	}



	public void onRowSelect(SelectEvent event) {
		secilenFirma = (Firma) event.getObject();
	}

	public void save() throws IOException {
		if (this.secilenFirma != null) {
						
			service.save(this.secilenFirma);
			addDetailMessage(secilenFirma.getIsim() + " Adlı Firma Kaydedildi");
			Faces.redirect("/musteri-kaydi/firma-list.jsf");
		}
	}

	public void showNewView() {
		this.secilenFirma = null;
		this.secilenFirma = new Firma();
	}

	
	public FirmaService getService() {
		return service;
	}


	public void setService(FirmaService service) {
		this.service = service;
	}


	public LazyDataModel<Firma> getFirmalar() {
		return firmalar;
	}


	public void setFirmalar(LazyDataModel<Firma> firmalar) {
		this.firmalar = firmalar;
	}


	public Firma getSecilenFirma() {
		return secilenFirma;
	}


	public void setSecilenFirma(Firma secilenFirma) {
		this.secilenFirma = secilenFirma;
	}
	
	
}
