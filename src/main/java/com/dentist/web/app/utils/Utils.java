package com.dentist.web.app.utils;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.omnifaces.util.Messages;

@Named
@ApplicationScoped
public class Utils implements Serializable {

	private static final long serialVersionUID = -4129630500236446980L;

	public static void addDetailMessage(String message) {
		addDetailMessage(message, null);
	}

	public static void addDetailMessage(String message, FacesMessage.Severity severity) {

		FacesMessage facesMessage = Messages.create("").detail(message).get();
		if (severity != null && severity != FacesMessage.SEVERITY_WARN) {
			facesMessage.setSeverity(severity);
		}
		Messages.add(null, facesMessage);
	}

}
