package com.dentist.web.app.dao.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.Payment;

public interface PaymentRepoitory extends JpaRepository<Payment, Integer> {

	List<Payment> findByPatientIdOrderByIdAsc(Integer id);

	List<Payment> findByPatientIdAndSign(Integer id, String sign);

	List<Payment> findByDoctorIdOrderByIdAsc(Integer doctorId);

	List<Payment> findBySign(String sign);

	Long deleteByRelatedToothInfoId(Integer relatedToothInfoId);

	List<Payment> findBySignAndDate(String sign, Date date);

	List<Payment> findByDoctorIdAndDateAndSign(Integer doctorId, Date date, String sign);

	List<Payment> findBySignAndDateBetween(String sign, Date date1, Date date2);

	List<Payment> findByDoctorIdAndDateBetweenAndSign(Integer doctorId, Date date1, Date date2, String sign);

	Page<Payment> findByDoctorId(Integer id, Pageable pageable);
}
