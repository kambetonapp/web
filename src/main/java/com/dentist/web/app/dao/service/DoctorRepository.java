package com.dentist.web.app.dao.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

	List<Doctor> findByName(String name);

	List<Doctor> findByNameContainsIgnoreCase(String query);

	Doctor findByUserName(String query);

	List<Doctor> findByBranch(String branch);
}
