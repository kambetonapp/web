package com.dentist.web.app.dao.model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author burakerken
 */

@Entity
@Table(name = "patient")
public class Patient implements Serializable {

	private static final long serialVersionUID = 7590009205523876983L;
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private Long tcNo;
	@Column
	private String name;
	@Column
	private Date birthDate;
	@Column
	private String phoneNumber;
	@Column
	private Integer doctor;
	@Column
	private Date registerDate;

	public Patient() {
	}

	public Patient(Long tcNo, String name, Date birthDate) {
		this.tcNo = tcNo;
		this.name = name;
		this.birthDate = birthDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Long getTcNo() {
		return tcNo;
	}

	public void setTcNo(Long tcNo) {
		this.tcNo = tcNo;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getDoctor() {
		return doctor;
	}

	public void setDoctor(Integer doctor) {
		this.doctor = doctor;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public boolean hasName() {
		return name != null && !"".equals(name.trim());
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", tcNo=" + tcNo + ", name=" + name + ", birthDate=" + birthDate + "]";
	}

}
