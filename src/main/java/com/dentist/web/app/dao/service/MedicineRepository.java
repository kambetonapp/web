package com.dentist.web.app.dao.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.Medicine;

public interface MedicineRepository extends JpaRepository<Medicine, Integer> {

	List<Medicine> findByNameContainsIgnoreCase(String query);

	Page<Medicine> findByNameContainsIgnoreCase(String name, Pageable pageable);

}
