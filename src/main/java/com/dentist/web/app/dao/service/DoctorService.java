package com.dentist.web.app.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.Doctor;

@Component
public class DoctorService {

	@Autowired
	DoctorRepository repository;

	public DoctorService() {
	}

	public Doctor save(Doctor d) {
		return repository.save(d);
	}

	public void delete(Doctor d) {
		repository.delete(d);
	}

	public Optional<Doctor> findById(Integer id) {
		return repository.findById(id);
	}

	public List<Doctor> findAll() {
		return repository.findAll();
	}

	public List<Doctor> getDoctors() {
		return repository.findByBranch("hekim");
	}

	public Page<Doctor> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<Doctor> findByName(String query) {
		return repository.findByName(query);
	}

	public List<Doctor> findByNameContains(String query) {
		return repository.findByNameContainsIgnoreCase(query);
	}

	public Doctor findByUserName(String query) {
		return repository.findByUserName(query);
	}

}
