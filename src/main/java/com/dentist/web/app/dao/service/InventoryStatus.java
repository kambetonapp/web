package com.dentist.web.app.dao.service;

public enum InventoryStatus {
	
	IN_STOCK,
    ORDERED,
    RESERVED,
    SHIPPED,
    DELIVERED, INSTOCK, LOWSTOCK, OUTOFSTOCK

}
