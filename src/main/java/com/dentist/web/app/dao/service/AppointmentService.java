package com.dentist.web.app.dao.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.Appointment;

@Component
public class AppointmentService {

	@Autowired
	AppointmentRepository repository;

	public Optional<Appointment> findById(Integer id) {
		return repository.findById(id);
	}

	public Appointment save(Appointment a) {
		return repository.save(a);
	}

	public void delete(Appointment a) {
		repository.delete(a);
	}

	public List<Appointment> findAll() {
		return repository.findAll();
	}

	public Page<Appointment> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<Appointment> findByDate(Date c) {
		return repository.findByDate(c);
	}

	public Page<Appointment> findByDoctorId(Integer doctorId, Pageable pageable) {
		return repository.findByDoctorId(doctorId, pageable);
	}

	public List<Appointment> findByDateAndDoctorId(Date c, Integer doctorId) {
		return repository.findByDateAndDoctorId(c, doctorId);
	}

	public List<Appointment> findByPatientId(Integer patientId) {
		return repository.findByPatientId(patientId);
	}

	public List<Appointment> findByDateBetween(Date dateBeginForIncome, Date dateEndForIncome) {
		return repository.findByDateBetween(dateBeginForIncome, dateEndForIncome);
	}

}
