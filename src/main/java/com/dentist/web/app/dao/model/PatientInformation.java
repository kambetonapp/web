package com.dentist.web.app.dao.model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "patient_information")
public class PatientInformation implements Serializable {

	private static final long serialVersionUID = 7590009205523876983L;

	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private Integer patientId;
	@Column
	private String cevap1;
	@Column
	private String cevap2;
	@Column
	private String cevap3;
	@Column
	private String cevap4;
	@Column
	private String cevap5;
	@Column
	private String cevap6;
	@Column
	private String cevap7;
	@Column
	private Boolean bool1;
	@Column
	private Boolean bool2;
	@Column
	private Boolean bool3;
	@Column
	private Boolean bool4;
	@Column
	private Boolean bool5;
	@Column
	private Boolean bool6;
	@Column
	private Boolean bool7;
	@Column
	private Boolean bool8;
	@Column
	private Boolean bool9;
	@Column
	private Boolean bool10;
	@Column
	private Boolean bool11;
	@Column
	private Boolean bool12;
	@Column
	private Boolean bool13;
	@Column
	private Boolean bool14;
	@Column
	private Boolean bool15;
	@Column
	private Boolean bool16;
	@Column
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		System.out.println(patientId + "set Edildi");
		this.patientId = patientId;
	}

	public PatientInformation() {

	}

	public String getCevap1() {
		return cevap1;
	}

	public void setCevap1(String cevap1) {
		this.cevap1 = cevap1;
		System.out.println(cevap1);
	}

	public String getCevap2() {
		return cevap2;

	}

	public void setCevap2(String cevap2) {
		this.cevap2 = cevap2;
		System.out.println(cevap2);
	}

	public String getCevap3() {
		return cevap3;

	}

	public void setCevap3(String cevap3) {
		this.cevap3 = cevap3;
		System.out.println(cevap3);
	}

	public String getCevap4() {
		return cevap4;
	}

	public void setCevap4(String cevap4) {
		this.cevap4 = cevap4;

		System.out.println(cevap4);
	}

	public String getCevap5() {
		return cevap5;
	}

	public void setCevap5(String cevap5) {
		this.cevap5 = cevap5;

		System.out.println(cevap5);
	}

	public String getCevap6() {
		return cevap6;

	}

	public void setCevap6(String cevap6) {
		this.cevap6 = cevap6;
		System.out.println(cevap6);

	}

	public String getCevap7() {
		return cevap7;

	}

	public void setCevap7(String cevap7) {
		this.cevap7 = cevap7;
		System.out.println(cevap7);
	}

	public Boolean getBool1() {
		return bool1;
	}

	public void setBool1(Boolean bool1) {
		this.bool1 = bool1;
	}

	public Boolean getBool2() {
		return bool2;
	}

	public void setBool2(Boolean bool2) {
		this.bool2 = bool2;
	}

	public Boolean getBool3() {
		return bool3;
	}

	public void setBool3(Boolean bool3) {
		this.bool3 = bool3;
	}

	public Boolean getBool4() {
		return bool4;
	}

	public void setBool4(Boolean bool4) {
		this.bool4 = bool4;
	}

	public Boolean getBool5() {
		return bool5;
	}

	public void setBool5(Boolean bool5) {
		this.bool5 = bool5;
	}

	public Boolean getBool6() {
		return bool6;
	}

	public void setBool6(Boolean bool6) {
		this.bool6 = bool6;
	}

	public Boolean getBool7() {
		return bool7;
	}

	public void setBool7(Boolean bool7) {
		this.bool7 = bool7;
	}

	public Boolean getBool8() {
		return bool8;
	}

	public void setBool8(Boolean bool8) {
		this.bool8 = bool8;
	}

	public Boolean getBool9() {
		return bool9;
	}

	public void setBool9(Boolean bool9) {
		this.bool9 = bool9;
	}

	public Boolean getBool10() {
		return bool10;
	}

	public void setBool10(Boolean bool10) {
		this.bool10 = bool10;
	}

	public Boolean getBool11() {
		return bool11;
	}

	public void setBool11(Boolean bool11) {
		this.bool11 = bool11;
	}

	public Boolean getBool12() {
		return bool12;
	}

	public void setBool12(Boolean bool12) {
		this.bool12 = bool12;
	}

	public Boolean getBool13() {
		return bool13;
	}

	public void setBool13(Boolean bool13) {
		this.bool13 = bool13;
	}

	public Boolean getBool14() {
		return bool14;
	}

	public void setBool14(Boolean bool14) {
		this.bool14 = bool14;
	}

	public Boolean getBool15() {
		return bool15;
	}

	public void setBool15(Boolean bool15) {
		this.bool15 = bool15;
	}

	public Boolean getBool16() {
		return bool16;
	}

	public void setBool16(Boolean bool16) {
		this.bool16 = bool16;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "PatientInformation [id=" + id + ", patientId=" + patientId + ", cevap1=" + cevap1 + ", cevap2=" + cevap2
				+ ", cevap3=" + cevap3 + ", cevap4=" + cevap4 + ", cevap5=" + cevap5 + ", cevap6=" + cevap6
				+ ", cevap7=" + cevap7 + ", bool1=" + bool1 + ", bool2=" + bool2 + ", bool3=" + bool3 + ", bool4="
				+ bool4 + ", bool5=" + bool5 + ", bool6=" + bool6 + ", bool7=" + bool7 + ", bool8=" + bool8 + ", bool9="
				+ bool9 + ", bool10=" + bool10 + ", bool11=" + bool11 + ", bool12=" + bool12 + ", bool13=" + bool13
				+ ", bool14=" + bool14 + ", bool15=" + bool15 + ", bool16=" + bool16 + ", description=" + description
				+ "]";
	}

}
