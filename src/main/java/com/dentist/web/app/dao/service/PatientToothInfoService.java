package com.dentist.web.app.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.PatientToothInfo;

@Component
public class PatientToothInfoService {

	@Autowired
	PatientToothInfoRepository repository;

	public PatientToothInfoService() {
	}

	public Optional<PatientToothInfo> findById(Integer id) {
		return repository.findById(id);
	}
	
	public List<PatientToothInfo> findByPatientId(Integer patientId){
		return repository.findByPatientId(patientId);
		
	}

	public Page<PatientToothInfo> findByPatientId(Integer patientId, Pageable page) {
		return repository.findByPatientId(patientId, page);
	}

	public PatientToothInfo save(PatientToothInfo p) {
		return repository.save(p);
	}

	public void delete(PatientToothInfo d) {
		repository.delete(d);
	}
}
