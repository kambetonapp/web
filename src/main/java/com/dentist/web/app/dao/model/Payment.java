package com.dentist.web.app.dao.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "payment")
public class Payment {
	@Id
	@GeneratedValue
	private Integer id;
	@Column
	private Integer doctorId;
	@Column
	private Integer patientId;
	@Column
	@Temporal(TemporalType.DATE)
	private Date date;
	@Column
	private Long toothId;
	@Column
	private String treatmentName;
	@Column
	private Integer amount;
	@Column
	private String sign;
	@Column
	private Integer relatedToothInfoId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	public Integer getPatientId() {
		return patientId;
	}

	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Long getToothId() {
		return toothId;
	}

	public void setToothId(Long toothId) {
		this.toothId = toothId;
	}

	public String getTreatmentName() {
		return treatmentName;
	}

	public void setTreatmentName(String treatmentName) {
		this.treatmentName = treatmentName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Integer getRelatedToothInfoId() {
		return relatedToothInfoId;
	}

	public void setRelatedToothInfoId(Integer relatedToothInfoId) {
		this.relatedToothInfoId = relatedToothInfoId;
	}

}
