package com.dentist.web.app.dao.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.Patient;

public interface PatientRepository extends JpaRepository<Patient, Integer> {

	List<Patient> findByName(String name);

	List<Patient> findByNameContainsIgnoreCase(String query);

	Page<Patient> findByTcNo(Long tcNo, Pageable pageable);

	Page<Patient> findByNameContainsIgnoreCase(String name, Pageable pageable);

	Page<Patient> findByDoctor(Integer doctorId, Pageable pageable);

}
