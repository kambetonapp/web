package com.dentist.web.app.dao.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.PatientToothInfo;

public interface PatientToothInfoRepository extends JpaRepository<PatientToothInfo, Integer> {

	Page<PatientToothInfo> findByPatientId(Integer patientId, Pageable page);

	List<PatientToothInfo> findByPatientId(Integer patientId);

}
