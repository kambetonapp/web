package com.dentist.web.app.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.Patient;

@Component
public class PatientService {

	@Autowired
	PatientRepository repository;

	public PatientService() {
	}

	public Patient save(Patient p) {
		return repository.save(p);
	}

	public void delete(Patient p) {
		repository.delete(p);
	}

	public Optional<Patient> findById(Integer id) {
		return repository.findById(id);
	}

	public List<Patient> findAll() {
		return repository.findAll();
	}

	public Page<Patient> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<Patient> findByName(String query) {
		return repository.findByName(query);
	}

	public List<Patient> findByNameContains(String query) {
		return repository.findByNameContainsIgnoreCase(query);
	}

	public Page<Patient> findByTcNo(Long tcNo, Pageable pageable) {
		return repository.findByTcNo(tcNo, pageable);
	}

	public Page<Patient> findByNameContainsIgnoreCase(String name, Pageable pageable) {
		return repository.findByNameContainsIgnoreCase(name, pageable);
	}

	public Page<Patient> findByDoctor(Integer doctorId, Pageable pageable) {
		return repository.findByDoctor(doctorId, pageable);
	}
}
