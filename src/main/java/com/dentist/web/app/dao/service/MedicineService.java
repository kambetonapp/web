package com.dentist.web.app.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.Medicine;

@Component
public class MedicineService {

	@Autowired
	MedicineRepository repository;

	public MedicineService() {
	}

	public Medicine save(Medicine m) {
		return repository.save(m);
	}

	public void delete(Medicine p) {
		repository.delete(p);
	}

	public Optional<Medicine> findById(Integer id) {
		return repository.findById(id);
	}

	public List<Medicine> findAll() {
		return repository.findAll();
	}

	public Page<Medicine> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<Medicine> findByNameContainsIgnoreCase(String query) {
		return repository.findByNameContainsIgnoreCase(query);
	}
}
