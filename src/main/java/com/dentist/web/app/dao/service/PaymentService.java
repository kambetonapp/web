package com.dentist.web.app.dao.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Payment;

@Component
public class PaymentService {
	@Autowired
	PaymentRepoitory repository;

	public PaymentService() {
	}

	public Payment save(Payment p) {
		return repository.save(p);
	}

	public void delete(Payment p) {
		repository.delete(p);
	}

	@Transactional
	public Long deleteByRelatedToothInfoId(Integer relatedToothInfoId) {
		return repository.deleteByRelatedToothInfoId(relatedToothInfoId);
	}

	public Optional<Payment> findById(Integer id) {
		return repository.findById(id);
	}

	public List<Payment> findAll() {
		return repository.findAll();
	}

	public List<Payment> findByPatientIdOrderByIdAsc(Integer id) {
		return repository.findByPatientIdOrderByIdAsc(id);
	}

	public List<Payment> findByPatientIdAndSign(Integer id, String sign) {
		return repository.findByPatientIdAndSign(id, sign);
	}

	public List<Payment> findByDoctorIdOrderByIdAsc(Integer doctorId) {
		return repository.findByDoctorIdOrderByIdAsc(doctorId);
	}

	public List<Payment> findBySign(String sign) {
		return repository.findBySign(sign);
	}

	public List<Payment> findBySignAndDate(String sign, Date date) {
		return repository.findBySignAndDate(sign, date);
	}

	public List<Payment> findBySignAndDateBetween(String sign, Date date1, Date date2) {
		return repository.findBySignAndDateBetween(sign, date1, date2);
	}

	public List<Payment> findByDoctorIdAndDateAndSign(Integer doctorId, Date date, String sign) {
		return repository.findByDoctorIdAndDateAndSign(doctorId, date, sign);
	}

	public List<Payment> findByDoctorIdAndDateBetweenAndSign(Integer doctorId, Date date1, Date date2, String sign) {
		return repository.findByDoctorIdAndDateBetweenAndSign(doctorId, date1, date2, sign);
	}
	
	public Page<Payment> findByDoctorId(Integer id, Pageable pageable) {
		return repository.findByDoctorId(id,pageable);
	}

}
