package com.dentist.web.app.dao.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.Appointment;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {

	List<Appointment> findByDate(Date c);

	List<Appointment> findByDateAndDoctorId(Date c, Integer doctorId);

	List<Appointment> findByPatientId(Integer patientId);

	Page<Appointment> findByDoctorId(Integer doctorId, Pageable pageable);

	List<Appointment> findByDateBetween(Date dateBeginForIncome, Date dateEndForIncome);
}
