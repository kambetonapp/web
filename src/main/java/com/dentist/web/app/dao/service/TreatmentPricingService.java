package com.dentist.web.app.dao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.TreatmentPricing;

@Component
public class TreatmentPricingService {

	@Autowired
	TreatmentPricingRepository repository;

	public TreatmentPricingService() {
	}

	public TreatmentPricing save(TreatmentPricing p) {
		return repository.save(p);
	}

	public void delete(TreatmentPricing p) {
		repository.delete(p);
	}

	public Optional<TreatmentPricing> findById(Integer id) {
		return repository.findById(id);
	}

	public List<TreatmentPricing> findAll() {
		return repository.findAll();
	}

	public Page<TreatmentPricing> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public List<TreatmentPricing> findByNameContainsIgnoreCase(String query) {
		return repository.findByNameContainsIgnoreCase(query);
	}
}
