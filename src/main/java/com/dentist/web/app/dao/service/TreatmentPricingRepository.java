package com.dentist.web.app.dao.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.TreatmentPricing;

public interface TreatmentPricingRepository extends JpaRepository<TreatmentPricing, Integer> {

	List<TreatmentPricing> findByNameContainsIgnoreCase(String query);

	Page<TreatmentPricing> findByNameContainsIgnoreCase(String name, Pageable pageable);

}
