package com.dentist.web.app.dao.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dentist.web.app.dao.model.PatientInformation;

public interface PatientInformationRepository extends JpaRepository<PatientInformation, Integer> {
	PatientInformation findByPatientId(Integer patientId);

}
