package com.dentist.web.app.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dentist.web.app.dao.model.PatientInformation;

@Component
public class PatientInformationService {

	@Autowired
	PatientInformationRepository repository;

	public PatientInformationService() {
	}

	public PatientInformation save(PatientInformation p) {
		return repository.save(p);
	}

	public PatientInformation findByPatientId(Integer patientId) {
		return repository.findByPatientId(patientId);
	}
}
