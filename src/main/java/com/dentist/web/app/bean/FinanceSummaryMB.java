package com.dentist.web.app.bean;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.service.AppointmentService;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PaymentService;

@Named
@ViewScoped
public class FinanceSummaryMB {

	@Autowired
	PaymentService paymentService;

	@Autowired
	AppointmentService appointmentService;

	List<Doctor> doctors;

	@Autowired
	private DoctorService doctorService;

	private Date dateBeginForIncome = new Date();

	private Date dateEndForIncome = new Date();

	private Date dateBeginOperationCost = new Date();

	private Date dateEndForOperationCost = new Date();;

	public FinanceSummaryMB() {
	}

	public Date getDateBeginForIncome() {
		return dateBeginForIncome;
	}

	public void setDateBeginForIncome(Date dateBeginForIncome) {
		this.dateBeginForIncome = dateBeginForIncome;
	}

	public Date getDateEndForIncome() {
		return dateEndForIncome;
	}

	public void setDateEndForIncome(Date dateEndForIncome) {
		this.dateEndForIncome = dateEndForIncome;
	}

	public Date getDateBeginOperationCost() {
		return dateBeginOperationCost;
	}

	public void setDateBeginOperationCost(Date dateBeginOperationCost) {
		this.dateBeginOperationCost = dateBeginOperationCost;
	}

	public Date getDateEndForOperationCost() {
		return dateEndForOperationCost;
	}

	public void setDateEndForOperationCost(Date dateEndForOperationCost) {
		this.dateEndForOperationCost = dateEndForOperationCost;
	}

	@PostConstruct
	public void init() {
		this.doctors = doctorService.getDoctors();
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	// Bugün yapılan işlemler sonucu alınması gereken tutardır.
	public Integer todaysIncome() {
		Integer todaysIncome = paymentService.findBySignAndDateBetween("+", dateBeginForIncome, dateEndForIncome)
				.stream().map(x -> x.getAmount()).reduce(0, Integer::sum);
		return todaysIncome;
	}

	// Bugün kasaya giren toplam tutardır.
	public Integer todaysOperationCost() {
		Integer todaysIncome = paymentService.findBySignAndDateBetween("-", dateBeginForIncome, dateEndForIncome)
				.stream().map(x -> x.getAmount()).reduce(0, Integer::sum);
		return todaysIncome;
	}

	// Bugünkü randevuların sayısı
	public Integer todaysPatientNumber() {
		return appointmentService.findByDateBetween(dateBeginForIncome, dateEndForIncome).size();

	}

	public Integer getDoctorsDailyGiro(Integer doctorId) {
		List<Payment> payments = paymentService.findByDoctorIdAndDateBetweenAndSign(doctorId, dateBeginOperationCost,
				dateEndForOperationCost, "+");
		int total = 0;
		for (Payment payment : payments) {
			total = payment.getAmount() + total;
		}
		return total;
	}
	
	public void redirect(Integer doctorId) {
		
	}
}
