package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.dentist.web.app.dao.model.TreatmentPricing;
import com.dentist.web.app.dao.service.TreatmentPricingService;

/**
 * @author @berken
 *
 */
@Named
@ViewScoped
public class TreatmentPricingListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	TreatmentPricingService service;

	LazyDataModel<TreatmentPricing> treatmentPricings;

	TreatmentPricing selectedTreatmentPricing;

	@PostConstruct
	public void initDataModel() {
		treatmentPricings = new LazyDataModel<TreatmentPricing>() {
			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<TreatmentPricing> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<TreatmentPricing> page;

				page = service.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<TreatmentPricing> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public TreatmentPricing getRowData(String key) {
				return service.findById(new Integer(key)).get();
			}
		};
	}

	public List<String> completeName(String query) {
		List<String> result = service.findByNameContainsIgnoreCase(query).stream().map(e -> e.getName())
				.collect(Collectors.toList());
		return result;
	}

	public void delete() throws IOException {
		service.delete(selectedTreatmentPricing);
		addDetailMessage(selectedTreatmentPricing.getName() + " isimli tedavi başarı ile silindi");
		selectedTreatmentPricing = new TreatmentPricing();
		Faces.redirect("/treatment-and-pricing-list.jsf");
	}

	public LazyDataModel<TreatmentPricing> getTreatmentPricings() {
		return treatmentPricings;
	}

	public void setTreatmentPricings(LazyDataModel<TreatmentPricing> treatmentPricings) {
		this.treatmentPricings = treatmentPricings;
	}

	public TreatmentPricing getSelectedTreatmentPricing() {
		return selectedTreatmentPricing;
	}

	public void setSelectedTreatmentPricing(TreatmentPricing selectedTreatmentPricing) {
		this.selectedTreatmentPricing = selectedTreatmentPricing;
	}

	public void onRowSelect(SelectEvent event) {
		selectedTreatmentPricing = (TreatmentPricing) event.getObject();
	}

	public void save() throws IOException {
		if (this.selectedTreatmentPricing != null) {
			service.save(this.selectedTreatmentPricing);
			addDetailMessage("Tedavi Kayıt Edildi");
			Faces.redirect("/treatment-and-pricing-list.jsf");
		}
	}

	public void showNewView() {
		this.selectedTreatmentPricing = null;
		this.selectedTreatmentPricing = new TreatmentPricing();
	}
}
