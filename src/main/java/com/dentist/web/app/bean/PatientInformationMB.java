package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.PatientInformation;
import com.dentist.web.app.dao.service.PatientInformationService;
import com.dentist.web.app.dao.service.PatientService;

@Named
@ViewScoped
public class PatientInformationMB {

	@Autowired
	private PatientInformationService service;

	@Autowired
	private PatientService patientService;

	private PatientInformation info;

	private Integer patientId;

	public PatientInformation getInfo() {
		if (info == null)
			info = new PatientInformation();
		return info;
	}

	public void setInfo(PatientInformation patientInformation) {
		this.info = patientInformation;
	}

	public void getInfoByPatientId(Integer patientId) {
		this.info = service.findByPatientId(patientId);
		this.patientId = patientId;
	}

	public void save() {
		service.save(info);
		String patientName = patientService.findById(patientId).get().getName();
		addDetailMessage(" - " + patientName + " - için Hasta Tedavi Kartı Bilgileri Başarı ile  Kayıt Edildi.");
	}

}