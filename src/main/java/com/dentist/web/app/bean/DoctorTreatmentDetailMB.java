package com.dentist.web.app.bean;

import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PaymentService;

@Named
@ViewScoped
public class DoctorTreatmentDetailMB implements Serializable {

	private static final long serialVersionUID = 4579930956596501661L;
	private Integer id;
	private Doctor doctor;
	boolean saved;

	LazyDataModel<Payment> payments;

	@Autowired
	DoctorService doctorService;

	@Autowired
	PaymentService paymentService;

	@PostConstruct
	public void init() throws IOException {

		System.out.println("Gelen id " + this.id);
		if (has(id) && id != 0) {
			doctor = doctorService.findById(id).get();
			initDataModel();
		} else {
			System.out.println("Doktro Bulunamadı");
		}
	}

	public void initDataModel() {

		payments = new LazyDataModel<Payment>() {

			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<Payment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Payment> page = paymentService.findByDoctorId(id,
						PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Payment> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Payment getRowData(String key) {
				return paymentService.findById(new Integer(key)).get();
			}
		};
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Payment> getPayments() {

		List<Payment> payments = paymentService.findByDoctorIdOrderByIdAsc(this.id);
		System.out.println(payments);
		return payments;
	}

}
