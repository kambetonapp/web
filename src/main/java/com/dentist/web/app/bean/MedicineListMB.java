package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.dentist.web.app.dao.model.Medicine;
import com.dentist.web.app.dao.service.MedicineService;

/**
 * @author @berken
 *
 */
@Named
@ViewScoped
public class MedicineListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	MedicineService service;

	LazyDataModel<Medicine> medicines;

	Medicine selectedMedicine;

	@PostConstruct
	public void initDataModel() {
		medicines = new LazyDataModel<Medicine>() {
			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<Medicine> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Medicine> page;

				page = service.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Medicine> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Medicine getRowData(String key) {
				return service.findById(new Integer(key)).get();
			}
		};
	}

	public List<String> completeName(String query) {
		List<String> result = service.findByNameContainsIgnoreCase(query).stream().map(e -> e.getName())
				.collect(Collectors.toList());
		return result;
	}

	public void delete() throws IOException {
		service.delete(selectedMedicine);
		addDetailMessage(selectedMedicine.getName() + " isimli ilaç başarı ile silindi");
		selectedMedicine = new Medicine();
		Faces.redirect("/medicine-list.jsf");
	}

	public LazyDataModel<Medicine> getMedicines() {
		return medicines;
	}

	public void setMedicines(LazyDataModel<Medicine> Medicines) {
		this.medicines = Medicines;
	}

	public Medicine getSelectedMedicine() {
		return selectedMedicine;
	}

	public void setSelectedMedicine(Medicine selectedMedicine) {
		this.selectedMedicine = selectedMedicine;
	}

	public void onRowSelect(SelectEvent event) {
		selectedMedicine = (Medicine) event.getObject();
	}

	public void save() throws IOException {
		if (this.selectedMedicine != null) {
			service.save(this.selectedMedicine);
			addDetailMessage("İlaç Kayıt Edildi");
			Faces.redirect("/medicine-list.jsf");
		}
	}

	public void showNewView() {
		this.selectedMedicine = null;
		this.selectedMedicine = new Medicine();
	}
}
