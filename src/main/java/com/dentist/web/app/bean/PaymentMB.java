package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.service.PatientService;
import com.dentist.web.app.dao.service.PaymentService;

@Named
@ViewScoped
public class PaymentMB {

	@Autowired
	private PaymentService service;
	@Autowired
	private PatientService patientService;

	private List<Payment> payments;

	private Payment selectedPayment;

	private String selectedPatientName;

	private Integer patientId;

	private Integer paidAmount;

	private String paymentMethod;

	private Integer discount;

	public PaymentMB() {

	}

	public Integer getDiscount() {
		return discount;
	}

	public void setDiscount(Integer discount) {
		this.discount = discount;
	}

	public List<Payment> getPayments() {
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	public Payment getSelectedPayment() {
		return selectedPayment;
	}

	public void setSelectedPayment(Payment selectedPayment) {
		this.selectedPayment = selectedPayment;
	}

	public String getSelectedPatientName() {
		return selectedPatientName;
	}

	public void setSelectedPatientName(String selectedPatientName) {
		this.selectedPatientName = selectedPatientName;
	}

	public Integer getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Integer paidAmount) {
		this.paidAmount = paidAmount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public void delete(Payment p) {
		this.service.delete(p);
		updatePayments(patientId);
	}

	public void updatePayments(Integer patientId) {
		this.patientId = patientId;
		this.selectedPatientName = patientService.findById(patientId).get().getName();
		this.payments = service.findByPatientIdOrderByIdAsc(patientId);
	}

	public String getTotalDept() {
		if (this.payments != null) {
			Integer operationAmount = 0;
			Integer payedAmount = 0;
			for (Payment payment : this.payments) {
				if (payment.getSign().equals("+")) {
					operationAmount = operationAmount + payment.getAmount();
				} else if (payment.getSign().equals("-")) {
					payedAmount = payedAmount + payment.getAmount();
				} else
					return "İşaret hatası bulunuyor";
			}
			return String.valueOf(operationAmount - payedAmount).concat(" ₺");
		} else {
			return "Bilgi Bulunamadı";
		}
	}

	public String getAmount(Payment p) {
		return p.getSign().equals("-") ? "-".concat(p.getAmount().toString()) : p.getAmount().toString();
	}

	public void savePaidAmount() {
		Payment payment = new Payment();
		payment.setPatientId(this.patientId);
		payment.setDate(new Date());
		payment.setTreatmentName("Alınan Ödeme :" + this.paymentMethod);
		payment.setAmount(this.paidAmount);
		payment.setSign("-");
		service.save(payment);
		updatePayments(this.patientId);
	}

	public void applyDiscount() {
		if (this.selectedPayment == null) {
			addDetailMessage("İndrim Uygulanacak Herangi Bir İşlem Bulunamadı.Herangi Bir İşlem Seçilmemiş",
					FacesMessage.SEVERITY_ERROR);
			return;
		}
		Integer amountBeforeDiscount = this.selectedPayment.getAmount();
		String sign = this.selectedPayment.getSign();

		if (sign.equals("-")) {
			addDetailMessage("Alınan Ödeme Üzerine İndirim Uygulanamaz. Lütfen Uygulanmış Bir Tedavi Seçiniz",
					FacesMessage.SEVERITY_ERROR);
			Faces.getFlash().setKeepMessages(true);
			return;
		}
		if (amountBeforeDiscount < this.discount) {
			addDetailMessage("İndirim Tutarı İşlem Tutarından Büyük Olamaz", FacesMessage.SEVERITY_ERROR);
			Faces.getFlash().setKeepMessages(true);
			return;
		}
		this.selectedPayment.setAmount(amountBeforeDiscount - this.discount);
		service.save(this.selectedPayment);
		addDetailMessage(this.discount + "₺ Tutarında İndirim Uygulandı", FacesMessage.SEVERITY_INFO);
		Faces.getFlash().setKeepMessages(true);
		updatePayments(this.selectedPayment.getPatientId());
	}

	public void onRowSelect(SelectEvent event) {
		this.selectedPayment = (Payment) event.getObject();
	}

	public String getStyleClass(Payment p) {
		if (p.getSign().equals("+"))
			return "label label-danger";
		else if (p.getSign().equals("-")) {
			return "label label-success";
		}
		return "";
	}

}
