package com.dentist.web.app.bean;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Patient;
import com.dentist.web.app.dao.model.PatientInformation;
import com.dentist.web.app.dao.model.PatientToothInfo;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PatientService;
import com.github.adminfaces.template.exception.BusinessException;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

/**
 * @author @berken
 *
 */
@Named
@ViewScoped
public class PatientListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	private LoginMB loginMB;

	@Autowired
	PatientService patientService;

	@Autowired
	PatientInformationMB patientInformationMB;

	@Autowired
	PatientToothInformationMB toothInformationMB;

	@Autowired
	PaymentMB paymentMB;

	@Autowired
	DoctorService doctorService;

	@Autowired
	private MediaViewMB mediaViewMB;

	@Autowired
	PanaromicImagesView panaromicImagesView;

	Integer id;

	LazyDataModel<Patient> patients;

	Patient selectedPatient;

	List<Patient> filteredValue;// datatable filteredValue attribute (column filters)

	@PostConstruct
	public void initDataModel() {
		patients = new LazyDataModel<Patient>() {
			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<Patient> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Patient> page;
				if (null != filters && filters.get("tcNo") != null)
					page = patientService.findByTcNo(Long.valueOf((String) (filters.get("tcNo"))),
							PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));
				else if (null != filters && filters.get("name") != null)
					page = patientService.findByNameContainsIgnoreCase((String) (filters.get("name")),
							PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));
				else {

					if (getCurrentPage().equals("/secretary.jsf"))
						page = patientService
								.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));
					else if (getCurrentPage().equals("/patient-list.jsf")
							&& (loginMB.getDoctor().getBranch().toLowerCase().equals("sekreter"))) {
						page = patientService
								.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

					} else if (getCurrentPage().equals("/patient-list.jsf")
							&& !(loginMB.getDoctor().getBranch().toLowerCase().equals("sekreter"))) {
						Integer doctorId = loginMB.getDoctor().getId();
						page = patientService.findByDoctor(doctorId,
								PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));
					} else {
						page = patientService
								.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));
					}
				}

				List<Patient> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Patient getRowData(String key) {
				return patientService.findById(new Integer(key)).get();
			}
		};
	}

	public List<String> completeName(String query) {
		List<String> result = patientService.findByNameContains(query).stream().map(e -> e.getName())
				.collect(Collectors.toList());
		return result;
	}

	public void findPatientById(Integer id) {
		if (id == null) {
			throw new BusinessException("Bulmak istediğiniz hastanın numarasını girin");
		}
		selectedPatient = patientService.findById(id).get();
	}

	public void delete() {
		patientService.delete(selectedPatient);
		addDetailMessage(selectedPatient.getName() + " isimli hasta başarı ile silindi");
		selectedPatient = new Patient();
	}

	public List<Patient> getFilteredValue() {
		return filteredValue;
	}

	public void setFilteredValue(List<Patient> filteredValue) {
		this.filteredValue = filteredValue;
	}

	public LazyDataModel<Patient> getPatients() {
		return patients;
	}

	public void setCars(LazyDataModel<Patient> patients) {
		this.patients = patients;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Patient getSelectedPatient() {
		return selectedPatient;
	}

	public void setSelectedPatient(Patient selectedPatient) {
		this.selectedPatient = selectedPatient;
	}

	public void setPatients(LazyDataModel<Patient> patients) {
		this.patients = patients;
	}

	public void onRowSelect(SelectEvent event) {
		selectedPatient = (Patient) event.getObject();
		Integer patientId = selectedPatient.getId();
		patientInformationMB.getInfoByPatientId(patientId);
		if (patientInformationMB.getInfo().getPatientId() == null)
			patientInformationMB.getInfo().setPatientId(selectedPatient.getId());

		toothInformationMB.setInfo(new PatientToothInfo());
		toothInformationMB.getInfo().setPatientId(selectedPatient.getId());
		toothInformationMB.getInfo().setDoctorId(loginMB.getDoctor().getId());
		toothInformationMB.initDataModel();

		paymentMB.setSelectedPayment(null);
		paymentMB.updatePayments(selectedPatient.getId());

		mediaViewMB.updateContent(patientService.findById(patientId).get(), loginMB.getDoctor());
		panaromicImagesView.setSelectedPatient(selectedPatient);
	}

	public String getCurrentPage() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		HttpServletRequest servletRequest = (HttpServletRequest) ctx.getExternalContext().getRequest();
		return servletRequest.getRequestURI();
	}

	public void print() throws DocumentException, IOException, InterruptedException {
		System.err.println(System.getProperty("user.dir"));
		if (this.getSelectedPatient() == null) {
			addDetailMessage("Lütfen bir hasta seçiniz", FacesMessage.SEVERITY_ERROR);

		} else {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream(
					System.getProperty("user.dir") + "/src/main/webapp/resources/media/HastaOnamFormu.pdf"));
			document.open();

			BaseFont bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1254", BaseFont.EMBEDDED);

			Font font = new Font(bf, 12);

			String onamMetni = new String("\r\n GENEL ONAM FORMU\r\n" + "\r\n"
					+ "Aşağıda imzası olan ben/hastanın vasisi '[hastaIsmi]' ,\r\n" + "\r\n"
					+ "Diş Hekimi ' [hekimIsmi] ' tarafından konulan tanı ve tedavi ile ilgili planlama, alternatif tedaviler, sonuçları, istenmeyen yan etkileri hakkında bilgilendirildim, anladım. Uygulanacak olan tedaviyi kabul ettim.\r\n"
					+ "\r\n"
					+ "Tedavi süresinde/sırasında ortaya çıkabilecek yeni durumlarla planlamanın değişebileceği anlatıldı, anladım ve kabul ettim.\r\n"
					+ "\r\n"
					+ "Tedavi uygulanmadığı takdirde ortaya çıkabilecek olası riskler, tedavimin alternatif uygulamalarına göre maliyet hesapları, gerekli görüldüğü takdirde diğer hekimlerden konsültasyon istenebileceği konularında bilgilendirildim, anladım, kabul ettim.\r\n"
					+ "\r\n"
					+ "Tedavim/ vasisi olduğum kişinin tedavisi hakkında merak ettiğim tüm sorulara cevap verildi. Yapılacak tedavilerin başarısının bana da bağlı olduğu, evde üzerime düşen ağız temizliği ve önerilere uymam gerektiği, vazgeçilmesi gereken zararlı alışkanlıklarla ilgili önerileri yerine getirme ve yazılacak reçetelerdeki ilaçları tarife uygun doz ve sürelerde kullanma gerekliliği anlatıldı,anladım ve kabul ettim.\r\n"
					+ "\r\n"
					+ "Uygulanacak tedavilerin ağız ve diş sağlığını korumayı amaçladığını, tıbbi hizmetlerin özenle yürütüleceği ancak tıbbi işlemlerde sonucun garanti edilemeyeceği tarafıma anlatıldı, anladım ve kabul ettim.\r\n"
					+ "\r\n"
					+ "Yukarıda belirtildiği gibi tedavi planlaması sırasında bana/vasisi olduğum kişiye anlatılan ve benim tarafımdan kabul edilen diş tedavilerini onayladım ve kabul ettim.\r\n"
					+ "\r\n"
					+ "Hasta hakları ve sorumlulukları, hekim hakları ve yükümlülükleri konularında detaylı olarak bilgilendirildim.\r\n"
					+ "\r\n"
					+ "Tedaviyi kabul ettikten sonra bana/vasisi olduğum kişiye ait radyografi, fotoğraf, video ve diğer dokümanların, eğitim ve/veya bilimsel amaçlı çalışmalarda anonimleştirilmiş veri olarak kullanılmasına izin veriyorum. Kişisel verilerimin Kamu kurum ve kuruluşları da dahil olmak üzere üçüncü kişi ve kurumlarla paylaşılmasına izin ......................... (El yazınız ile “veriyorum” ya da “vermiyorum” yazınız.)\r\n"
					+ "\r\n"
					+ " .................................................................. El yazınız ile “okuduğumu anladım, kabul ediyorum” yazınız.\r\n"
					+ "\r\n" + " Tarih: [tarih] \r\n" + "\r\n" + "\r\n" + "\r\n" + "\r\n");
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			LocalDate date = LocalDate.now();
			String now = date.format(formatter);
			onamMetni = onamMetni.replace("[hastaIsmi]", this.getSelectedPatient().getName());
			Optional<Doctor> doctor = this.doctorService.findById(this.getSelectedPatient().getDoctor());
			doctor.get();
			onamMetni = onamMetni.replace("[hekimIsmi]", doctor.get().getName());
			onamMetni = onamMetni.replace("[tarih]", now);
			onamMetni = onamMetni.replace("[hastaTc]", this.getSelectedPatient().getTcNo().toString());
			onamMetni = onamMetni.replace("[hastaTel]", this.getSelectedPatient().getPhoneNumber());

			document.add(new Paragraph(onamMetni, font));
			addPatientCardInfo(document);
			document.close();

			Thread.sleep(100l);

			PrimeFaces.current().executeScript("PF('onamFormuPdf').show()");
			PrimeFaces.current().executeScript("PF('onamFormuPdf').update()");
		}
	}

	public void addPatientCardInfo(Document document) throws DocumentException, IOException {

		String text = new String("\r\n HASTA KARTI \r\n" + "\r\n"
				+ "1- Şu anda herhangi bir tedavi görüyor musunuz? İlaç kullanıyor musunuz?\r\n" + "-[cevap1]\r\n"
				+ "2- Baş ve boyun bölgesinde radyoterapi görüdnüz mü?\r\n" + "-[cevap2]\r\n"
				+ "3- Cerrahi müdehale veya yaralanma sonrası kanama uzun sürer mi?\r\n" + "-[cevap3]\r\n"
				+ "4- Bunların dışında herhangi bir tıbbi sorununuz var mı?\r\n" + "-[cevap4]\r\n"
				+ "5- Sürekli kontrolünde olduğunuz doktorunuz var mı?\r\n" + "-[cevap5]\r\n"
				+ "6- Bayanlarda- Hamilelik, düşük adet ve menapoz bilgileri:\r\n" + "-[cevap6]\r\n"
				+ "7- Herhangi bir hastalığınız var mı ? Lütfen seçiniz.\r\n" + "-[cevap7]\r\n	\r\n"
				+ "[cb1] Kalp Hastalıkları\r\n" + "[cb2] Şeker Hastalığı\r\n" + "[cb3] Tansiyon Sorunu\r\n"
				+ "[cb4] Epilepsi (sara)\r\n" + "[cb5]Ateşli romatizma\r\n" + "[cb6] Eklem romatizması\r\n"
				+ "[cb7] Guatr (Trold tabletleri)\r\n" + "[cb8] Kan Hastalıkları\r\n" + "[cb9] İlaç alerjisi\r\n"
				+ "[cb10] Zührevi hastalık\r\n" + "[cb11] Sarılık\r\n" + "[cb12] Astım, saman nezlesi\r\n"
				+ "[cb13] Böbrek karaciğer bozuklukları\r\n" + "[cb14] Akciğer hastalıkları \r\n"
				+ "[cb15] Sinüzit \r\n " + "[cb16] AIDS \r\n " + "Açıklama : [description]");

		PatientInformation info = this.patientInformationMB.getInfo();
		text = text.replace("[cevap1]", info.getCevap1());
		text = text.replace("[cevap2]", info.getCevap2());
		text = text.replace("[cevap3]", info.getCevap3());
		text = text.replace("[cevap4]", info.getCevap4());
		text = text.replace("[cevap5]", info.getCevap5());
		text = text.replace("[cevap6]", info.getCevap6());
		text = text.replace("[cevap7]", info.getCevap7());
		text = text.replace("[description]", info.getDescription());

		text = text.replace("[cb1]", getCheckBusAsTickOrX(info.getBool1()));
		text = text.replace("[cb2]", getCheckBusAsTickOrX(info.getBool2()));
		text = text.replace("[cb3]", getCheckBusAsTickOrX(info.getBool3()));
		text = text.replace("[cb4]", getCheckBusAsTickOrX(info.getBool4()));
		text = text.replace("[cb5]", getCheckBusAsTickOrX(info.getBool5()));
		text = text.replace("[cb6]", getCheckBusAsTickOrX(info.getBool6()));
		text = text.replace("[cb7]", getCheckBusAsTickOrX(info.getBool7()));
		text = text.replace("[cb8]", getCheckBusAsTickOrX(info.getBool8()));
		text = text.replace("[cb9]", getCheckBusAsTickOrX(info.getBool9()));
		text = text.replace("[cb10]", getCheckBusAsTickOrX(info.getBool10()));
		text = text.replace("[cb11]", getCheckBusAsTickOrX(info.getBool11()));
		text = text.replace("[cb12]", getCheckBusAsTickOrX(info.getBool12()));
		text = text.replace("[cb13]", getCheckBusAsTickOrX(info.getBool13()));
		text = text.replace("[cb14]", getCheckBusAsTickOrX(info.getBool14()));
		text = text.replace("[cb15]", getCheckBusAsTickOrX(info.getBool15()));
		text = text.replace("[cb16]", getCheckBusAsTickOrX(info.getBool16()));

		BaseFont bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, "Cp1254", BaseFont.EMBEDDED);

		Font font = new Font(bf, 10);

		document.add(new Paragraph(text, font));

		String imFile = System.getProperty("user.dir") + "/src/main/webapp/resources/images/disTablo.jpg";

		Image image = Image.getInstance(imFile);
		document.add(image);
	}

	public String getCheckBusAsTickOrX(Boolean bool) {
		String yes = "[ var ]";
		String no = "[ x ]";
		String ret = null;
		ret = bool == true ? yes : no;
		return ret;
	}

	public String getIdFile() {
		return java.util.UUID.randomUUID().toString();
	}
}
