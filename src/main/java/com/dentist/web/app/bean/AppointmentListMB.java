package com.dentist.web.app.bean;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.PostConstruct;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import com.dentist.web.app.dao.model.Appointment;
import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.service.AppointmentService;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PatientService;
import com.dentist.web.app.dao.service.PaymentService;

/**
 * @author @berken
 *
 */
@Named
@ViewScoped
public class AppointmentListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	AppointmentService appointmentService;

	@Autowired
	PatientService patientService;

	@Autowired
	DoctorService doctorService;

	@Autowired
	private LoginMB loginMB;
	
	@Autowired
	private PaymentService paymentService;

	Integer id;

	LazyDataModel<Appointment> appointments;

	Appointment selectedAppointment;

	List<Doctor> filteredValue;

	Integer selectedDoctorId;

	@PostConstruct
	public void initDataModel() {

		this.appointments = new LazyDataModel<Appointment>() {

			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<Appointment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				Integer doctorId = loginMB.getDoctor().getId();
				Sort sort = Sort.by(Sort.Order.desc("date"), Sort.Order.asc("time"));

				Page<Appointment> page = null;

				if (loginMB.getDoctor().getBranch().equals("sekreter")
						|| loginMB.getDoctor().getBranch().equals("admin")) {

					if (selectedDoctorId == null || selectedDoctorId == 0)
						page = appointmentService.findAll(PageRequest.of(first / pageSize, pageSize, sort));
					else
						page = appointmentService.findByDoctorId(selectedDoctorId,
								PageRequest.of(first / pageSize, pageSize, sort));
				} else {
					if (selectedDoctorId == null || selectedDoctorId == 0)
						page = appointmentService.findByDoctorId(doctorId,
								PageRequest.of(first / pageSize, pageSize, sort));
					else
						page = appointmentService.findByDoctorId(selectedDoctorId,
								PageRequest.of(first / pageSize, pageSize, sort));

				}

				List<Appointment> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Appointment getRowData(String key) {
				return appointmentService.findById(new Integer(key)).get();
			}
		};
	}

	public void delete() throws IOException {
		appointmentService.delete(selectedAppointment);
		deleteDependentPayment(selectedAppointment);
		selectedAppointment = null;
		Faces.redirect("appointment-list.jsf");
	}
	
	private void deleteDependentPayment(Appointment appointment) {
		
			Payment payment = new Payment();
			payment.setDoctorId(appointment.getDoctorId());
			payment.setPatientId(appointment.getPatientId());
			payment.setDate(appointment.getDate());
			payment.setTreatmentName("Muayene İptali");
			payment.setAmount(20);
			payment.setSign("-");
			paymentService.save(payment);
	}

	public List<Doctor> getFilteredValue() {
		return filteredValue;
	}

	public void setFilteredValue(List<Doctor> filteredValue) {
		this.filteredValue = filteredValue;
	}

	public LazyDataModel<Appointment> getAppointments() {
		return appointments;
	}

	public Appointment getSelectedAppointment() {
		return selectedAppointment;
	}

	public void setSelectedAppointment(Appointment selectedAppointment) {
		this.selectedAppointment = selectedAppointment;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSelectedDoctorId() {
		return selectedDoctorId;
	}

	public void setSelectedDoctorId(Integer selectedDoctorId) {
		this.selectedDoctorId = selectedDoctorId;
	}

	public void setAppointments(LazyDataModel<Appointment> appointments) {
		this.appointments = appointments;
	}

	public String findPatientById(Integer patientId) {
		if (patientId != null)
			return this.patientService.findById(patientId).get().getName();
		else
			return "";
	}

	public String isAppointmentOld(Appointment a) throws ParseException {
		if (a != null && a.getDate() != null && a.getTime() != null) {

			Date date = a.getDate();
			String time = a.getTime();
			Date now = Calendar.getInstance().getTime();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
			String appointmentDateString = formatter2.format(date);

			Date appointmentDate = formatter.parse(appointmentDateString + " " + time);

			if (appointmentDate.compareTo(now) < 0) {
				return "label label-warning";
			}
			return "label label-success";
		} else
			return "";
	}

	public String findDoctorById(Integer doctorId) {

		try {
			if (doctorId != null)
				return "Dr. ".concat(this.doctorService.findById(doctorId).get().getName());
			else
				return "";
		} catch (NoSuchElementException e) {
			return "Doktor Bulunamadı";
		}

	}

	public void onRowSelect(SelectEvent event) {
		selectedAppointment = (Appointment) event.getObject();
	}

	public List<Doctor> getDoctors() {
		return doctorService.getDoctors();
	}

	public void onDoctorSelect(AjaxBehaviorEvent e) {
		initDataModel();
	}

}
