package com.dentist.web.app.bean;

import com.dentist.web.app.dao.model.Patient;
import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.primefaces.shaded.commons.io.FilenameUtils;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

@Named
@SessionScoped
public class PanaromicImagesView {

	private UploadedFile file;

	private Patient selectedPatient;

	private String currentPicture;

	private String selectedPicture;

	private StreamedContent image;

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public String getCurrentPicture() {
		return currentPicture;
	}

	public void setCurrentPicture(String currentPicture) {
		this.currentPicture = currentPicture;
	}

	public Patient getSelectedPatient() {
		return this.selectedPatient;
	}

	public void setSelectedPatient(Patient selectedPatient) {
		this.selectedPatient = selectedPatient;
	}

	public List<String> fileNames() {
		Patient p = this.selectedPatient;
		if (p != null) {

			String pathString = System.getProperty("user.home")
					+ "/panaromik/".concat(p.getTcNo().toString());

			File folder = new File(pathString);

			File[] listOfFiles = folder.listFiles();

			List<String> fileNames = new ArrayList<String>();
			if (listOfFiles != null) {

				for (File file : listOfFiles) {
					fileNames.add(file.getName());
				}

				for (int i = 0, j = fileNames.size() - 1; i < j; i++) {
					fileNames.add(i, fileNames.remove(j));
				}
				return fileNames;
			}
		}
		return new ArrayList<String>();

	}

	public void fileUploadListener(FileUploadEvent e) throws InterruptedException {
		Patient p = this.selectedPatient;

		if (p != null) {
			String pathString = System.getProperty("user.home")
					+ "/panaromik/".concat(p.getTcNo().toString());

			Path path = Paths.get(pathString);

			try {
				Files.createDirectories(path);
				InputStream input = e.getFile().getInputstream();
				String filename = FilenameUtils.getBaseName(e.getFile().getFileName());
				String extension = FilenameUtils.getExtension(e.getFile().getFileName());

				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
				String date = formatter.format(new Date());
				File targetFile = new File(pathString + "/" + date + "-" + filename + "." + extension);

				FileUtils.copyInputStreamToFile(input, targetFile);

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			addDetailMessage("Başarı ile Yüklendi");
		} else {
			addDetailMessage("Dosya Yüklenirken Hata Oluştu", FacesMessage.SEVERITY_ERROR);
		}

	}

	public void changePicture(String imageName) {
		Patient p = this.selectedPatient;
		this.selectedPicture = p.getTcNo().toString().concat("/").concat(imageName);
		String pathString = "resources/panaromik/".concat(p.getTcNo().toString());
		pathString = System.getProperty("user.home").concat("/panaromik/").concat(p.getTcNo().toString()).concat("/");
		this.currentPicture = pathString.concat(imageName);

	}

	public void delete() {
		if(selectedPicture !=null) {

			File file = new File(currentPicture);
			try {
				boolean res = Files.deleteIfExists(file.toPath());
				if (res == true)
					addDetailMessage("Dosya Başarı ile Silindi");

				this.currentPicture = "";
			} catch (IOException e) {
				addDetailMessage("Dosya Silinirken Hata Oluştu", FacesMessage.SEVERITY_ERROR);
			}
		} else {
			addDetailMessage("Bir Resim Seçiniz", FacesMessage.SEVERITY_ERROR);
		}
	}

	public StreamedContent getImage() throws IOException {
		Patient p = this.selectedPatient;
		if (p == null) {
			return null;
		}
		FacesContext context = FacesContext.getCurrentInstance();

		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			// So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
			return new DefaultStreamedContent();
		} else {
			String imageName = context.getExternalContext().getRequestParameterMap().get("imageName");
			if(!(imageName != null && !imageName.trim().equals(""))) {
				return null;
			}
			File file = new File(imageName);
			byte[] imageBytes = Files.readAllBytes(file.toPath());
			return new DefaultStreamedContent(new ByteArrayInputStream(imageBytes));
		}
	}
}
