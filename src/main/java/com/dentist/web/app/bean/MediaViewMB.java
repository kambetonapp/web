package com.dentist.web.app.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Medicine;
import com.dentist.web.app.dao.model.Patient;
import com.dentist.web.app.dao.service.MedicineService;
import com.itextpdf.io.source.ByteArrayOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

@Named
@Scope(value = "session")
public class MediaViewMB {

	private static final Logger logger = LoggerFactory.getLogger(MediaViewMB.class);

	@Autowired
	private MedicineService medicineService;

	private String patientName;

	private String doctorName;

	private String medicineName;

	private String description;

	private StreamedContent file;

	private Patient patient;

	private Integer doctorId;

	private Integer selectedMedicineId;

	private List<Medicine> medicineList = new ArrayList<Medicine>();

	public StreamedContent getFile() {
		return file;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Integer getSelectedMedicineId() {
		return selectedMedicineId;
	}

	public void setSelectedMedicineId(Integer selectedMedicineId) {
		this.selectedMedicineId = selectedMedicineId;
	}

	public List<Medicine> getMedicineList() {
		return medicineList;
	}

	public void setMedicineList(List<Medicine> medicineList) {
		this.medicineList = medicineList;
	}

	public void init() {
		byte[] generatedReport;
		this.file = null;
		try {

			BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, "Cp1254", BaseFont.EMBEDDED);

			Font font = new Font(bf, 18);
			Font fontForRecete = new Font(bf, 16);

			Document document = new Document();

			ByteArrayOutputStream myStream = new ByteArrayOutputStream();

			PdfWriter.getInstance(document, myStream);

			document.open();

			Paragraph header = new Paragraph("LALE AĞIZ DİŞ SAĞLIĞI POLİKLİNİĞİ", font);
			header.setAlignment(Element.ALIGN_CENTER);
			header.setSpacingAfter(40f);
			document.add(header);

			Paragraph recete = new Paragraph("REÇETE", fontForRecete);
			recete.setAlignment(Element.ALIGN_CENTER);
			recete.setSpacingAfter(10f);
			document.add(recete);

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			LocalDate date = LocalDate.now();
			String now = date.format(formatter);

			addCellPair(document, "Hastanın Adı / Soyadı", this.patient.getName());
			addCellPair(document, "Tarih", now);
			addCellPair(document, "T.C.Kimlik No", this.patient.getTcNo().toString());
			addCellPair(document, "Tanı ", this.description);

			Paragraph ilaclar = new Paragraph("İLAÇLAR", fontForRecete);
			ilaclar.setAlignment(Element.ALIGN_LEFT);
			ilaclar.setSpacingBefore(10f);
			ilaclar.setSpacingAfter(10f);

			document.add(ilaclar);

			for (Medicine medicine : this.medicineList) {
				document.add(new Paragraph(medicine.getName()));

			}

			document.close();

			generatedReport = myStream.toByteArray();

//			generatedReport = (byte[]) index.read("byte_01");
			InputStream stream = new ByteArrayInputStream(generatedReport);
			this.file = new DefaultStreamedContent(stream, "pdf");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ilgili Belge Açılamadı", e);
		}
	}

	private Document addCellPair(Document document, String cell1String, String cell2String)
			throws DocumentException, IOException {

		BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, "Cp1254", BaseFont.EMBEDDED);

		Font font = new Font(bf, 10);

		PdfPTable table = new PdfPTable(2); // 3 columns.
		table.setWidthPercentage(100); // Width 100%

		// Set Column widths
		float[] columnWidths = { 2f, 4f };
		table.setWidths(columnWidths);

		PdfPCell cell1 = new PdfPCell(new Paragraph("Cell 1"));
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		cell1.addElement(new Paragraph(cell1String, font));

		PdfPCell cell2 = new PdfPCell(new Paragraph("Cell 2"));
//		cell2.setPaddingLeft(10);
		cell2.setPadding(10);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		cell2.addElement(new Paragraph(cell2String, font));

		table.addCell(cell1);
		table.addCell(cell2);

		document.add(table);
		return document;
	}

	public List<Medicine> getMedicines() {
		return medicineService.findAll();
	}

	public void addMedicine() {
		if (this.selectedMedicineId == null) {
			return;
		}
		this.medicineList.add(medicineService.findById(this.selectedMedicineId).get());
	}

	public void updateContent(Patient p, Doctor d) {
		this.patient = p;
		this.doctorId = d.getId();
	}

	public void selected(AjaxBehaviorEvent e) {
	}

	public void olustur() {

		System.err.println("doctorId : " + this.doctorId);
		System.err.println("patientId" + this.patient.getId());
		System.err.println("tanı" + this.description);
		System.err.println("ilaclar" + this.medicineList);
	}
}
