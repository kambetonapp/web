package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.service.DoctorService;

@Named
@ViewScoped
public class DoctorFormMB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Doctor doctor;
	boolean saved;

	@Autowired
	DoctorService doctorService;

	@PostConstruct
	public void init() {

		// if ajax request : return
		if (has(id) && id != 0) {
			doctor = doctorService.findById(id).get();
		} else {
			doctor = new Doctor();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public boolean isSaved() {
		return !(doctor == null || doctor.getId() == null);
	}

	public void remove() throws IOException {
		if (has(doctor) && has(doctor.getId())) {
			doctorService.delete(doctor);
			addDetailMessage(doctor.getName() + " başarı ile silindi");
			Faces.getFlash().setKeepMessages(true);
			this.doctor = null;
			Faces.redirect("doctor-list.jsf");
		}
	}

	public void save() throws IOException {
		String msg;
		if (doctor.getId() == null) {
			doctorService.save(doctor);
			msg = doctor.getName() + " başarı ile kayıt edildi";
		} else {
			doctorService.save(doctor);
			msg = doctor.getName() + " başarı ile güncellendi";
		}
		addDetailMessage(msg);
//		Faces.getFlash().setKeepMessages(true);
		Faces.redirect("doctor-list.jsf");
	}

	public void clear() {
		doctor = new Doctor();
		id = null;
	}

}
