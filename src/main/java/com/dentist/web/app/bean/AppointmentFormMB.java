package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.util.Faces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Appointment;
import com.dentist.web.app.dao.model.Patient;
import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.service.AppointmentService;
import com.dentist.web.app.dao.service.PaymentService;

@Named
@ViewScoped
public class AppointmentFormMB {

	private Appointment appointment;

	@Autowired
	private PatientListMB patientListMB;

	@Autowired
	private AppointmentService service;

	@Autowired
	private PaymentService paymentService;

	@PostConstruct
	void init() {
		date = new Date();
		this.appointments = service.findByDateAndDoctorId(date, this.getPatient().getDoctor());

		times = new ArrayList<String>();
		for (Appointment appointment : appointments) {
			times.add(appointment.getTime());
		}
	}

	private Date date;

	private String time;

	private List<Appointment> appointments;

	private List<String> times;

	private Integer controlDoctorId;

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Patient getPatient() {
		return this.patientListMB.getSelectedPatient();
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Integer getControlDoctorId() {
		return controlDoctorId;
	}

	public void setControlDoctorId(Integer controlDoctorId) {
		this.controlDoctorId = controlDoctorId;
	}

	public void onDateSelect(SelectEvent event) {
		if (this.controlDoctorId == null) {
			this.appointments = service.findByDateAndDoctorId((Date) event.getObject(), this.getPatient().getDoctor());
		} else
			this.appointments = service.findByDateAndDoctorId((Date) event.getObject(), this.controlDoctorId);

		times = new ArrayList<String>();
		for (Appointment appointment : appointments) {
			times.add(appointment.getTime());
		}
	}

	public void onControlDoctorSelect() {

		this.appointment = null;
		this.date = null;
		this.time = null;
		this.times = null;

		date = new Date();
		this.appointments = service.findByDateAndDoctorId(date, getControlDoctorId());
		times = new ArrayList<String>();
		for (Appointment appointment : appointments) {
			times.add(appointment.getTime());
		}

	}

	public boolean isSelected(String componentTime) {
		if (times != null) {
			if (times.contains(componentTime)) {
				return true;
			} else
				return false;
		}
		return true;

	}

	public boolean validate() {
		if (patientListMB.getSelectedPatient().getId() != null && patientListMB.getSelectedPatient().getDoctor() != null
				&& getDate() != null && getTime() != null)
			return true;
		else
			return false;

	}

	public void save() {
		appointment = new Appointment();
		appointment.setPatientId(getPatient().getId());
		if (this.controlDoctorId == null) {
			appointment.setDoctorId(getPatient().getDoctor());
		} else
			appointment.setDoctorId(this.controlDoctorId);
		appointment.setDate(getDate());
		appointment.setTime(getTime());
		if (validate()) {
			appointment = service.save(appointment);
			initialTreatmentCost();
			addDetailMessage(getPatient().getName() + " için saat :" + time
					+ "e randevu oluşturuldu. Düzenlemeleri Randevular Menüsünden Yapabilirsiniz");
		} else {
			addDetailMessage(
					"Randevu Oluşturulurken hata oluştu. Lütfen Hasta için hekim seçtiğinizden ve saat-tarih seçimini doğru yaptığığnızdan emin olunuz",
					FacesMessage.SEVERITY_ERROR);
		}

		try {
			if (getCurrentPage().equals("/patient-list.jsf")) {
				Faces.redirect("/patient-list.jsf");
			} else {
				Faces.redirect("/secretary.jsf");
			}

		} catch (IOException e) {
			addDetailMessage("Sayfa Yönlendirmesi Sırasında Hata Oluştu", FacesMessage.SEVERITY_ERROR);
		}
	}

	public String getCurrentPage() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		HttpServletRequest servletRequest = (HttpServletRequest) ctx.getExternalContext().getRequest();
		return servletRequest.getRequestURI();
	}

	private void initialTreatmentCost() {
		Payment payment = new Payment();
		payment.setDoctorId(appointment.getDoctorId());
		payment.setPatientId(appointment.getPatientId());
		payment.setDate(appointment.getDate());
		payment.setTreatmentName("Muayene Ücreti");
		payment.setAmount(20);
		payment.setSign("+");
		paymentService.save(payment);
	}

	public void handleClose(CloseEvent event) {
		this.appointment = null;
		this.date = null;
		this.time = null;
		this.times = null;
	}

}
