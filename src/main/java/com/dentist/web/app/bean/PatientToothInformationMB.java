package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.PatientToothInfo;
import com.dentist.web.app.dao.model.Payment;
import com.dentist.web.app.dao.model.TreatmentPricing;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PatientService;
import com.dentist.web.app.dao.service.PatientToothInfoService;
import com.dentist.web.app.dao.service.PaymentService;
import com.dentist.web.app.dao.service.TreatmentPricingService;

@Named
@ViewScoped
public class PatientToothInformationMB {

	@Autowired
	private TreatmentPricingService treatmentPricingService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PatientToothInfoService service;

	@Autowired
	private PaymentService paymentService;

	private Long selectedToothId;

	private PatientToothInfo info;

	LazyDataModel<PatientToothInfo> lastOperations;

	private List<PatientToothInfo> operations;

	private PatientToothInfo selectedOperation;

	private Integer treatmentId;

	private List<Integer> selectedTooths;

	private Integer doctorId;

	private Boolean isChild = Boolean.FALSE;

	public void initDataModel() {

		operations = service.findByPatientId(getInfo().getPatientId());

		lastOperations = new LazyDataModel<PatientToothInfo>() {

			private static final long serialVersionUID = 3408809510999238461L;

			@Override
			public List<PatientToothInfo> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				Page<PatientToothInfo> page = service.findByPatientId(getInfo().getPatientId(),
						PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<PatientToothInfo> list = page.getContent();
				setRowCount((int) page.getTotalElements());
				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public PatientToothInfo getRowData(String key) {
				return service.findById(new Integer(key)).get();
			}
		};
	}

	@PostConstruct
	public void init() {
		selectedTooths = new ArrayList<Integer>();

	}

	public Long getSelectedToothId() {
		return selectedToothId;
	}

	public List<Integer> getSelectedTooths() {
		return selectedTooths;
	}

	public void setSelectedTooths(List<Integer> selectedTooths) {
		this.selectedTooths = selectedTooths;
	}

	public void setSelectedToothId(Long selectedToothId) {
		this.selectedToothId = selectedToothId;
		if (!this.selectedTooths.contains(selectedToothId.intValue()))
			this.selectedTooths.add(selectedToothId.intValue());
		else {
			if (this.selectedTooths.contains(selectedToothId.intValue())) {
				this.selectedTooths.remove(this.selectedTooths.indexOf(selectedToothId.intValue()));
			}
		}
	}

	public PatientToothInfo getInfo() {
		if (info == null)
			info = new PatientToothInfo();
		return info;
	}

	public void setInfo(PatientToothInfo info) {
		this.info = info;
	}

	public LazyDataModel<PatientToothInfo> getLastOperations() {
		return lastOperations;
	}

	public void setLastOperations(LazyDataModel<PatientToothInfo> lastOperations) {
		this.lastOperations = lastOperations;
	}

	public PatientToothInfo getSelectedOperation() {
		return selectedOperation;
	}

	public void setSelectedOperation(PatientToothInfo selectedOperation) {
		this.selectedOperation = selectedOperation;
	}

	public List<PatientToothInfo> getOperations() {
		return operations;
	}

	public void setOperations(List<PatientToothInfo> operations) {
		this.operations = operations;
	}

	public Integer getTreatmentId() {
		return treatmentId;
	}

	public void setTreatmentId(Integer treatmentId) {
		this.treatmentId = treatmentId;
	}

	public Integer getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Integer doctorId) {
		this.doctorId = doctorId;
	}

	public Boolean getIsChild() {
		return isChild;
	}

	public void setIsChild(Boolean isChild) {
		this.isChild = isChild;
	}

	public String findPatientById(Integer patientId) {
		return this.patientService.findById(patientId).get().getName();
	}

	public String findDoctorById(Integer doctorId) {
		return "Dr. ".concat(this.doctorService.findById(doctorId).get().getName());
	}

	public String findTreatmentById(Integer treatmentId) {
		try {
			Optional<TreatmentPricing> optionalTreatmentPricing = treatmentPricingService.findById(treatmentId);
			return optionalTreatmentPricing.get().getName();
		} catch (Exception e) {
			return "İşlem Bulunamadı";
		}

	}

	public void delete(PatientToothInfo info) {
		this.service.delete(info);
		paymentService.deleteByRelatedToothInfoId(info.getId());
		this.operations = service.findByPatientId(getInfo().getPatientId());
	}

	public Integer findTreatmentPriceById(Integer treatmentId) {
		try {
			Optional<TreatmentPricing> optionalTreatmentPricing = treatmentPricingService.findById(treatmentId);
			return optionalTreatmentPricing.get().getPrice();
		} catch (Exception e) {
			return 0;
		}

	}

	public boolean isSelected(Long toothId) {
		boolean result = false;
		if (selectedToothId != null) {
			result = this.selectedTooths.contains(toothId.intValue());
		}
		return result;
	}

	public List<TreatmentPricing> getTreatments() {
		return this.treatmentPricingService.findAll();
	}

	public void onDoctorSelect(AjaxBehaviorEvent event) {
		getInfo().setDoctorId(getDoctorId());
	}


	public boolean validate() {
		boolean result = true;

		if (info.getPatientId() == null) {
			addDetailMessage("Lütfen Bir Hasta Seçiniz", FacesMessage.SEVERITY_ERROR);
			result = false;
		} else {
			Doctor doctor = doctorService.findById(getInfo().getDoctorId()).get();

			if (doctor.getBranch().toLowerCase().equals("sekreter")
					|| doctor.getBranch().toLowerCase().equals("admin")) {
				addDetailMessage("Lütfen İşlemi Yapan Doktoru Seçiniz", FacesMessage.SEVERITY_ERROR);
				result = false;
			}
		}
		if (selectedToothId == null) {
			addDetailMessage("Lütfen işlem yapmak istediğiniz diş numarasını seçiniz", FacesMessage.SEVERITY_ERROR);
			result = false;
		}
		if (getTreatmentId() == null) {
			addDetailMessage("Lütfen Yapılan İşlemi Seçiniz", FacesMessage.SEVERITY_ERROR);
			result = false;
		}
		return result;
	}

	public void save() throws IOException {
		if (validate()) {
			Integer patientId = getInfo().getPatientId();
			Integer doctorId = getInfo().getDoctorId();
			getInfo().setSelectedToothId(selectedToothId);
			getInfo().setDate(new Date());
			getInfo().setTreatmentName(findTreatmentById(getTreatmentId()));
			getInfo().setPrice(findTreatmentPriceById(getTreatmentId()));
			for (Integer toothId : this.selectedTooths) {
				this.info.setSelectedToothId(toothId.longValue());
				PatientToothInfo newInfo = new PatientToothInfo();
				newInfo.setDate(info.getDate());
				newInfo.setDescription(info.getDescription());
				newInfo.setDoctorId(info.getDoctorId());
				newInfo.setPatientId(info.getPatientId());
				newInfo.setPrice(info.getPrice());
				newInfo.setSelectedToothId(info.getSelectedToothId());
				newInfo.setTreatmentName(info.getTreatmentName());
				PatientToothInfo savedInfo = this.service.save(newInfo);

				// Reflect To Payment Table

				Payment payment = new Payment();
				payment.setDoctorId(this.info.getDoctorId());
				payment.setPatientId(this.info.getPatientId());
				payment.setDate(this.info.getDate());
				payment.setToothId(this.info.getSelectedToothId());
				payment.setTreatmentName(this.info.getTreatmentName());
				payment.setAmount(this.info.getPrice());
				payment.setSign("+");
				payment.setRelatedToothInfoId(savedInfo.getId());
				paymentService.save(payment);
			}


			info = null;
			info = new PatientToothInfo();
			info.setPatientId(patientId);
			info.setDoctorId(doctorId);
			initDataModel();
			addDetailMessage("İşlem Başarı ile Kayıt Kayıt Edildi");
		}
	}

}
