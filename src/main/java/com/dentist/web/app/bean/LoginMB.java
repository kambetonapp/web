package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.service.DoctorService;
import com.github.adminfaces.template.session.AdminSession;

/**
 * Created by rmpestano on 12/20/14.
 *
 * This is just a login example.
 *
 * AdminSession uses isLoggedIn to determine if user must be redirect to login
 * page or not. By default AdminSession isLoggedIn always resolves to true so it
 * will not try to redirect user.
 *
 * If you already have your authorization mechanism which controls when user
 * must be redirect to initial page or logon you can skip this class.
 */
@Named
@SessionScoped
@Primary
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoginMB extends AdminSession implements Serializable {

	@Autowired
	DoctorService doctorService;

	private static final long serialVersionUID = -2701681972090083516L;
	private String currentUser;
	private String email;
	private String password;
	private boolean remember;
	private Doctor doctor;

	public void login() throws IOException {

		Doctor validUser = doctorService.findByUserName(email);
		doctor = validUser;
		if (validUser != null && email.equals((String) validUser.getUserName())
				&& password.equals((String) validUser.getPassword()) && validUser.getUserName().equals("admin")) {

			currentUser = validUser.getUserName();
			addDetailMessage(validUser.getName() + " admin olarak giriş yaptınız");
			Faces.getExternalContext().getFlash().setKeepMessages(true);
			Faces.redirect("admin-index.jsf");
		} else if (validUser != null && email.equals((String) validUser.getUserName())
				&& password.equals((String) validUser.getPassword())) {
			currentUser = validUser.getUserName();
			Faces.redirect("index.jsf");
		} else {
			currentUser = null;
			addDetailMessage("Kullanıcı Adı yada Parola Hatalı", FacesMessage.SEVERITY_ERROR);
		}
	}

	@Override
	public boolean isLoggedIn() {

		return currentUser != null;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRemember() {
		return remember;
	}

	public void setRemember(boolean remember) {
		this.remember = remember;
	}

	public String getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getMenuLayoutAccordingToRole() {
		if (doctor.getUserName().equals("admin") || doctor.getUserName().equals("burak.erken")
				|| doctor.getBranch().toLowerCase().equals("admin"))
			return "/WEB-INF/util/adminMenu.xhtml";
		else if (doctor.getBranch().toLowerCase().equals("sekreter")) {
			return "/WEB-INF/util/secretaryMenu.xhtml";
		} else
			return "/WEB-INF/util/doctorMenu.xhtml";
	}

}
