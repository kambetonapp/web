package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.service.DoctorService;
import com.github.adminfaces.template.exception.BusinessException;

/**
 * @author @berken
 *
 */
@Named
@ViewScoped
public class DoctorListMB implements Serializable {

	private static final long serialVersionUID = -7691027261063479188L;

	@Autowired
	DoctorService doctorService;

	Integer id;

	LazyDataModel<Doctor> doctors;

	Doctor selectedDoctor;

	List<Doctor> filteredValue;// datatable filteredValue attribute (column filters)

	// Gerekirse Sıralama orderları eklenebilir.
	@PostConstruct
	public void initDataModel() {

		doctors = new LazyDataModel<Doctor>() {

			private static final long serialVersionUID = 7533805566716843927L;

			@Override
			public List<Doctor> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				Page<Doctor> page = doctorService
						.findAll(PageRequest.of(first / pageSize, pageSize, Sort.by(Direction.DESC, "id")));

				List<Doctor> list = page.getContent();

				setRowCount((int) page.getTotalElements());

				return list;
			}

			@Override
			public int getRowCount() {
				return super.getRowCount();
			}

			@Override
			public Doctor getRowData(String key) {
				return doctorService.findById(new Integer(key)).get();
			}
		};
	}

	public List<String> completeName(String query) {
		List<String> result = doctorService.findByNameContains(query).stream().map(e -> e.getName())
				.collect(Collectors.toList());
		return result;
	}

	public void findPatientById(Integer id) {
		if (id == null) {
			throw new BusinessException("Bulmak istediğiniz hastanın numarasını girin");
		}
		selectedDoctor = doctorService.findById(id).get();
	}

	public void delete() {
		doctorService.delete(selectedDoctor);
		addDetailMessage(selectedDoctor.getName() + " isimli hasta başarı ile silindi");
		selectedDoctor = new Doctor();
	}

	public List<Doctor> getFilteredValue() {
		return filteredValue;
	}

	public void setFilteredValue(List<Doctor> filteredValue) {
		this.filteredValue = filteredValue;
	}

	public LazyDataModel<Doctor> getDoctors() {
		return doctors;
	}

	public void setCars(LazyDataModel<Doctor> doctors) {
		this.doctors = doctors;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Doctor getSelectedDoctor() {
		return selectedDoctor;
	}

	public void setSelectedDoctor(Doctor selectedDoctor) {
		this.selectedDoctor = selectedDoctor;
	}

	public void setDoctors(LazyDataModel<Doctor> doctors) {
		this.doctors = doctors;
	}

	public void onRowSelect(SelectEvent event) {
		selectedDoctor = (Doctor) event.getObject();
	}

}
