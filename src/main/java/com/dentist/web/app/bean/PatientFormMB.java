package com.dentist.web.app.bean;

import static com.dentist.web.app.utils.Utils.addDetailMessage;
import static com.github.adminfaces.template.util.Assert.has;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import org.omnifaces.util.Faces;
import org.springframework.beans.factory.annotation.Autowired;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.model.Patient;
import com.dentist.web.app.dao.service.DoctorService;
import com.dentist.web.app.dao.service.PatientService;

/**
 * Created by @berken on 15.01.2020. patient-form.jsf sayfasının bean'idir.
 */
@Named
@ViewScoped
public class PatientFormMB implements Serializable {

	private static final long serialVersionUID = 2057222688048224512L;
	private Integer id;
	private Patient patient;
	boolean saved;

	@Autowired
	PatientService patientService;

	@Autowired
	DoctorService doctorService;

	@PostConstruct
	public void init() {

		// if ajax request : return
		if (has(id) && id != 0) {
			patient = patientService.findById(id).get();
		} else {
			patient = new Patient();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public boolean isSaved() {
		return !(patient == null || patient.getId() == null);
	}

	public void remove() throws IOException {
		if (has(patient) && has(patient.getId())) {
			patientService.delete(patient);
			addDetailMessage(patient.getName() + " başarı ile silindi");
			Faces.getFlash().setKeepMessages(true);
			this.patient = null;
			Faces.redirect("secretary.jsf");
		}
	}

	public void save() throws IOException {
		String msg;
		if (patient.getId() == null) {
			patient.setRegisterDate(new Date());
			patientService.save(patient);
			msg = patient.getName() + " başarı ile kayıt edildi";
		} else {
			patientService.save(patient);
			msg = patient.getName() + " başarı ile güncellendi";
		}
		addDetailMessage(msg);
		Faces.getFlash().setKeepMessages(true);
		Faces.redirect("secretary.jsf");
	}

	public List<Doctor> getDoctors() {
		return doctorService.getDoctors();
	}

	public void clear() {
		patient = new Patient();
		id = null;
	}

}
