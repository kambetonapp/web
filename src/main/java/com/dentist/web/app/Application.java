/*
 * Copyright 2016-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dentist.web.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.dentist.web.app.dao.model.Doctor;
import com.dentist.web.app.dao.service.DoctorService;

@EntityScan("com")
@EnableJpaRepositories("com")
@ComponentScan(basePackages = "com.github.adminfaces,com.dentist,com.vassaf.core")
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Application.class, args);

		DoctorService doctorService = context.getBean(DoctorService.class);

		Doctor admin = doctorService.findByUserName("admin");
		if (admin != null) {
			admin.setBranch("admin");
			doctorService.save(admin);
		}
		if (doctorService.findAll().size() < 1) {
			Doctor doctor = new Doctor();
			doctor.setName("admin");
			doctor.setUserName("admin");
			doctor.setPassword("h12w3");
			doctor.setBranch("admin");
			doctorService.save(doctor);
		}

	}
}
